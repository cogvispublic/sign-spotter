import argparse
import os
import pandas as pd
from itertools import groupby
from pathlib import Path

def filter_predictions(spotting_file,
         output_dir,
         confidence,
         fps):

    Path(output_dir).mkdir(parents=True, exist_ok=True)

    video_name = spotting_file.split("/")[-1]
    spottings_df = pd.read_csv(spotting_file, sep=',')

    # threshold by confidence
    filtered_df = spottings_df.loc[spottings_df.confidence >= confidence]
    # search for max confidence in each separate blobs.
    filtered_df = filtered_df.loc[filtered_df.groupby((filtered_df['predicted_gloss'].shift() != filtered_df['predicted_gloss']).cumsum())['confidence'].idxmax()]

    # sort by frame
    filtered_df = filtered_df.sort_values(by=['start_frame'])
    filtered_df['second'] = filtered_df['start_frame']/fps

    # change order of the columns
    filtered_df = filtered_df.reindex(columns=['start_frame', 'second', 'predicted_gloss', 'confidence'])

    filtered_df.to_csv(f"{output_dir}/th{confidence}_{video_name}", sep='|', index=False)

if __name__ == "__main__":
    p = argparse.ArgumentParser(description="Helper script to run demo.")
    p.add_argument(
        "--spotting_file",
        type=str,
        default= f"output/pred_glosses_confidences_/1180706_1a1.csv",
        help="A csv file contains spottings.",
    )
    p.add_argument(
        "--output_dir",
        type=str,
        default= f"output/pred_glosses_confidences_filtered",
        help="Path to output dir.",
    )
    p.add_argument(
        "--confidence",
        type=float,
        default=0.5,
        help="Only show predictions above certain confidence threshold",
    )
    p.add_argument(
        "--fps",
        type=str,
        default= 50,
        help="Fps information",
    )
    print(p.parse_args())
    filter_predictions(**vars(p.parse_args()))
