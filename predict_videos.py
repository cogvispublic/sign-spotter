import os
import sys
import math
import pickle as pkl
import argparse
from pathlib import Path

import cv2
import numpy as np
import torch
import scipy.special

#sys.path.append("..")
import models.i3d as models
from utils.misc import to_torch
from utils.imutils import im_to_numpy, im_to_torch, resize_generic
from utils.transforms import color_normalize
from utils.get_output_basename import get_output_basename
from filter_predictions import filter_predictions

import torchvision.transforms as transforms
import torchvision.transforms.functional as F
import json
import lzma
import pickle
import pathlib
from torchvision.utils import save_image

def load_rgb_video(video_path: Path, video_start_frame: int, resize_res: int, batch_size: int, num_in_frames) -> torch.Tensor:

    videofile = os.path.join(video_path)
    cap = cv2.VideoCapture(videofile)
    cap.set(cv2.CAP_PROP_POS_FRAMES, video_start_frame)  # cv2.cv2.CAP_PROP_POS_FRAMES
    cap_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    cap_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    video_end_frame = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    if cap_width > cap_height:  # crop from the sides
        center_crop = transforms.CenterCrop((cap_height, cap_height))
    elif cap_height > cap_width:
        center_crop = transforms.CenterCrop((cap_width, cap_width))

    f = 0
    rgb = []
    bound = min(num_in_frames + batch_size - 1, video_end_frame - video_start_frame)
    for i, f in enumerate(range(0, bound)): # since stride = 1, I added (batch_size -1)
        # frame: BGR, (h, w, 3), dtype=uint8 0..255
        ret, frame = cap.read()
        if not ret:
            print("Could not read: ", videofile)
            print("i:", i)
            break
        # BGR (OpenCV) to RGB (Torch)
        frame = frame[:, :, [2, 1, 0]]
        rgb_t = im_to_torch(frame)
        if cap_width != cap_height:
            rgb_t = center_crop(rgb_t)

        rgb_t = F.resize(rgb_t, resize_res)
        rgb.append(rgb_t)

    cap.release()

    # (nframes, 3, cap_height, cap_width) => (3, nframes, cap_height, cap_width)
    rgb = torch.stack(rgb).permute(1, 0, 2, 3)
    return rgb



def prepare_input(
    rgb: torch.Tensor,
    resize_res: int = 256,
    inp_res: int = 224,
    mean: torch.Tensor = 0.5 * torch.ones(3), std=1.0 * torch.ones(3),
):
    """
    Process the video:
    1) Resize to [resize_res x resize_res]
    2) Center crop with [inp_res x inp_res]
    3) Color normalize using mean/std
    """
    iC, iF, iH, iW = rgb.shape
    rgb_resized = np.zeros((iF, resize_res, resize_res, iC))
    for t in range(iF):
        tmp = rgb[:, t, :, :]
        tmp = resize_generic(
            im_to_numpy(tmp), resize_res, resize_res, interp="bilinear", is_flow=False
        )
        rgb_resized[t] = tmp
    rgb = np.transpose(rgb_resized, (3, 0, 1, 2))
    # Center crop coords
    ulx = int((resize_res - inp_res) / 2)
    uly = int((resize_res - inp_res) / 2)
    # Crop 256x256
    rgb = rgb[:, :, uly : uly + inp_res, ulx : ulx + inp_res]
    rgb = to_torch(rgb).float()
    #assert rgb.max() <= 1
    rgb = color_normalize(rgb, mean, std)
    return rgb



def load_model(
        checkpoint_path: Path,
        num_classes: int,
        num_in_frames: int,
        include_embds: int,
) -> torch.nn.Module:

    model = models.InceptionI3d(
        num_classes=num_classes,
        spatiotemporal_squeeze=True,
        final_endpoint="Logits",
        name="inception_i3d",
        in_channels=3,
        dropout_keep_prob=0.5,
        num_in_frames=num_in_frames,
        activation_func="swish",
        include_embds=include_embds
    )
    model = torch.nn.DataParallel(model).cuda()
    checkpoint = torch.load(checkpoint_path)
    model.load_state_dict(checkpoint["state_dict"],  strict=True)
    model.eval()
    return model

def load_vocab(word_data_json):
    with open(word_data_json, "r") as read_file:
        word_data = json.load(read_file)
    return word_data



def sliding_windows(
        rgb: torch.Tensor,
        num_in_frames: int,
        stride: int,
) -> tuple:
    """
    Return sliding windows and corresponding (middle) timestamp
    """
    C, nFrames, H, W = rgb.shape
    # If needed, pad to the minimum clip length
    if nFrames < num_in_frames:
        rgb_ = torch.zeros(C, num_in_frames, H, W)
        rgb_[:, :nFrames] = rgb
        rgb_[:, nFrames:] = rgb[:, -1].unsqueeze(1)
        rgb = rgb_
        nFrames = rgb.shape[1]

    num_clips = math.ceil((nFrames - num_in_frames) / stride) + 1

    rgb_slided = torch.zeros(num_clips, 3, num_in_frames, H, W)
    t_mid = []
    # For each clip
    for j in range(num_clips):
        # Check if num_clips becomes 0
        actual_clip_length = min(num_in_frames, nFrames - j * stride)
        if actual_clip_length == num_in_frames:
            t_beg = j * stride
        else:
            t_beg = nFrames - num_in_frames
        t_mid.append(t_beg + num_in_frames / 2)
        rgb_slided[j] = rgb[:, t_beg : t_beg + num_in_frames, :, :]
    return rgb_slided, np.array(t_mid)


def main(
    checkpoint_path: Path,
    dataset_path: Path,
    output_dir: str,
    start_frame_input: int,
    end_frame_input:int,
    word_data_json,
    num_classes: int,
    num_in_frames: int,
    batch_size: int,
    stride: int,
    topk: int,
    resize_res: int,
    datasetname: str,
    save_embds: int,
    save_prob_vectors: int,
    confidence: float
):

    model = load_model(
        checkpoint_path=checkpoint_path,
        num_classes=num_classes,
        num_in_frames=num_in_frames,
        include_embds=True
    )
    word_data = load_vocab(word_data_json=word_data_json)

    video_list = []
    for path, subdirs, files in os.walk(dataset_path):
        for file in files:
            video_list.append(path + "/" + file)

    for each_video in video_list:
        frame_pred_score = []  # information for start frame, which class is predicted, how much confidence. for ex: [1 gloss_name 0.84]

        video_path = each_video
        video_end_frame = end_frame_input

        if video_end_frame == 0:
            videofile = os.path.join(video_path)
            cap = cv2.VideoCapture(videofile)
            FPS = int(cap.get(cv2.CAP_PROP_FPS))
            if not cap.isOpened():
                print(video_path)
                raise('video does not exist')
            else:
                print(video_path)

            video_end_frame = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
            print(f"{video_end_frame} frames.\n")

        feature_dim = 1024
        all_preds = np.zeros((video_end_frame - num_in_frames + 1, num_classes))
        all_embds = np.zeros((video_end_frame - num_in_frames + 1, feature_dim))

        video_name = get_output_basename(video_path, datasetname)

        if os.path.isfile(f"{output_dir}/pred_glosses_confidences/{video_name}.csv"):
            continue

        for start_frame in range(start_frame_input, video_end_frame-num_in_frames, stride + (batch_size-1)):
            rgb_orig = load_rgb_video(
                    video_path=Path(video_path),
                    video_start_frame=start_frame,
                    num_in_frames=num_in_frames,
                    resize_res=resize_res,
                    batch_size=batch_size
                )
            # Prepare: resize/crop/normalize
            rgb_input = prepare_input(rgb_orig, resize_res)
            # Sliding window
            rgb_slides, t_mid = sliding_windows(
                rgb=rgb_input,
                stride=stride,
                num_in_frames=num_in_frames,
            )
            # Number of windows/clips
            num_clips = rgb_slides.shape[0]
            # Group the clips into batches
            num_batches = math.ceil(num_clips / batch_size)
            raw_scores = np.empty((0, num_classes), dtype=float)
            embds = np.empty((0, feature_dim), dtype=float)
            for b in range(num_batches):
                inp = rgb_slides[b * batch_size : (b + 1) * batch_size]
                # Forward pass
                out = model(inp)
                raw_scores = np.append(raw_scores, out["logits"].cpu().detach().numpy(), axis=0)
                embds = np.append(embds, out["embds"].cpu().detach().numpy().squeeze(4).squeeze(3).squeeze(2), axis=0)

            prob_scores = scipy.special.softmax(raw_scores, axis=1)
            prob_sorted = np.sort(prob_scores, axis=1)[:, ::-1]
            pred_sorted = np.argsort(prob_scores, axis=1)[:, ::-1]

            # Predicted gloss names are appended to frame_pred_score[]
            word_topk = np.empty((topk, num_clips), dtype=object)
            for k in range(topk):
                for i, p in enumerate(pred_sorted[:, k]):
                    word_topk[k, i] = word_data["words"][p]
            prob_topk = prob_sorted[:, :topk].transpose()

            for b in range(num_clips):
                if save_prob_vectors:
                    all_preds[start_frame + b] = prob_scores[b]
                if save_embds:
                    all_embds[start_frame + b] = embds[b]
                # video clips contain num_in_frames (16) frames. So, assume that mid point is the predicted time. So add int(num_in_frames/2) as well.
                frame_pred_score.append([start_frame + b + int(num_in_frames/2), word_topk[0][b], round(prob_topk[0][b], 2)])

        # Save predicted glosses and their confidence
        pathlib.Path(output_dir + "/pred_glosses_confidences").mkdir(parents=True, exist_ok=True)
        np.savetxt(f"{output_dir}/pred_glosses_confidences/{video_name}.csv",
                   frame_pred_score,
                   delimiter=",",
                   fmt='%s',
                   header="start_frame,predicted_gloss,confidence",
                   comments='')

        if save_prob_vectors:
            pathlib.Path(output_dir + "/probs").mkdir(parents=True, exist_ok=True)
            out_file = os.path.join(output_dir + "/probs", video_name + ".lzma.pkl")
            all_preds = all_preds.astype(np.float16)
            with lzma.open(out_file, "wb") as f:
                pickle.dump(all_preds, f)

        if save_embds:
            pathlib.Path(output_dir + "/embds").mkdir(parents=True, exist_ok=True)
            out_file = os.path.join(output_dir + "/embds", video_name + ".lzma.pkl")
            all_embds = all_embds.astype(np.float16)
            with lzma.open(out_file, "wb") as f:
                pickle.dump(all_embds, f)

        # now filter predictions
        filter_predictions(spotting_file=f"{output_dir}/pred_glosses_confidences/{video_name}.csv",
                           output_dir= output_dir + "/pred_glosses_confidences_filtered",
                           confidence=confidence,
                           fps=FPS)

        # Note:
        # To load saved pkl file:
        # with lzma.open(out_file, 'rb') as file:
        #     raw_data = file.read()
        #     data = pickle.loads(raw_data)

if __name__ == "__main__":
    p = argparse.ArgumentParser(description="Helper script to run demo.")
    p.add_argument(
        "--checkpoint_path",
        type=Path,
        default= "checkpoints/meinedgs_i3d.pth.tar",
        help="Path to checkpoint.",
    )
    p.add_argument(
        "--word_data_json",
        type=Path,
        default="meinedgs_vocab.json",
        help="A path to a json file contains class ids and class (gloss) names.",
    )
    p.add_argument(
        "--dataset_path",
        type=Path,
        default= "videos",
        help="Path to dataset. The code will process all the videos in the directory.",
    )
    p.add_argument(
        "--output_dir",
        type=str,
        default= "output",
        help="Path to save outputs. All predictions and filtered predictions (over confidence threshold) will be saved.",
    )
    p.add_argument(
        "--start_frame_input",
        type=int,
        default=0,
        help="Start frame of the video.",
    )
    p.add_argument(
        "--end_frame_input",
        type=int,
        default=0,
        help="End frame of the video. Default=0, in this case whole video is predicted.",
    )
    p.add_argument(
        "--num_in_frames",
        type=int,
        default=16,
        help="Number of frames processed at a time by the model. This should be 16.",
    )
    p.add_argument(
        "--stride",
        type=int,
        default=1,
        help="Number of frames to stride.",
    )
    p.add_argument(
        "--batch_size",
        type=int,
        default=4,
        help="Maximum number of clips to put in each batch",
    )
    p.add_argument(
        "--num_classes",
        type=int,
        default=2301,
        help="The number of classes predicted by the model. 2301 for DGS-I3D, 2281 for BSL-I3D.",
    )
    p.add_argument(
        "--topk", type=int, default=1, help="Top-k results to show.",
    )
    p.add_argument(
        "--resize_res",
        type=int,
        default=224,
        help="Spatial resolution of the network input. This should be 224.",
    )
    p.add_argument(
        "--datasetname",
        type=str,
        default= "",
        help=" Output name is determined according to the dataset's directory and filename structure. It is done via get_output_basename() function, please feel free to edit it.",
    )
    p.add_argument(
        "--save_embds",
        type=int,
        default=0,
        help="Whether to save the I3D embeddings. Each frame has 1024 dimension.",
    )
    p.add_argument(
        "--save_prob_vectors",
        type=int,
        default=0,
        help="Whether to save I3D class probabilities. Each frame has num_classes dimension.",
    )
    p.add_argument(
        "--confidence",
        type=float,
        default=0.5,
        help="A threshold between 0 and 1. Only predictions above this threshold will be taken and post-processed to create spotted glosses across the video. It will be saved into output_dir/pred_glosses_confidences_filtered.",
    )
    print(p.parse_args())
    main(**vars(p.parse_args()))
