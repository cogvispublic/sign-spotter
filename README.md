# Sign Spotter
Sign Spotter is a tool designed to extract sign language representation automatically from sign language videos. It leverages the I3D (Inflated 3D ConvNet) architecture, to achieve robust and accurate sign language recognition. 

The tool uses a sliding window over videos and saves the following:
- spatio-temporal features as pkl files,
- class probabilities as csv files,
- spotted glosses as csv files.

## Setup
```
conda create -n spotter_env python=3.9
conda env update -n spotter_env --file environment.yml
conda activate spotter_env
conda install pytorch==1.12.1 torchvision==0.13.1 torchaudio==0.12.1 cudatoolkit=11.3 -c pytorch
```

## I3D
We have pre-trained an I3D model on two different sign language datasets: [BOBSL](https://www.robots.ox.ac.uk/~vgg/data/bobsl/) (British Sign Language) and [MeineDGS](https://www.sign-lang.uni-hamburg.de/meinedgs/ling/start_en.html) (German Sign Language). 

I3D code is based on this github repo: [bsl1k](https://github.com/gulvarol/bsl1k), but has been modified. Please refer to Section III-A of [our paper](https://arxiv.org/abs/2403.10434) for more details of the I3D classifier.

## Running `predict_videos.py`

```
python predict_videos.py <arguments>
```

Here are some of the arguments that you should update based on your preferences.

- `--checkpoint_path`: Full path to model weights.
- `--word_data_json`: A path to a json file containing class ids and class (gloss) names.
- `--num_classes`: The number of classes predicted by the model. 2301 for DGS-I3D, 2281 for BSL-I3D.
- `--datasetname`: This argument affects the output filenames. The *get_output_basename()* function uses the *datasetname* to construct the output base filename based on your dataset's structure. Please customize *get_output_basename()*.
- `--dataset_path`: Path to the input directory. The code will process all the videos in the directory.
- `--output_dir`: Path to save output files. The following subfolders will be created: 
   - __output_dir/pred_glosses_confidences__ contains a csv file with predictions for each frame.
    - __output_dir/pred_glosses_confidences_filtered__ contains a csv file with only predictions over a threshold (`--confidence`)
    - __output_dir/embs__ contains I3D embeddings as numpy array with (T-15, 1024) dimension. T is the total number of frames in the video.
- `--confidence`: A threshold between 0 and 1. Only predictions above this threshold will be post-processed to create spotted glosses across the video. It will be saved in __output_dir/pred_glosses_confidences_filtered__.
- `--save_embds`: 0 or 1. 0 not to save embeddings, 1 otherwise.

To run DGS-I3D:
```
python predict_videos.py --checkpoint_path checkpoints/meinedgs_i3d.pth.tar --word_data_json vocabs/meinedgs_vocab.json --num_classes 2301 <other_arguments>
```
To run BSL-I3D:
```
python predict_videos.py --checkpoint_path checkpoints/bobsl_i3d.pth.tar --word_data_json vocabs/bobsl_vocab.json --num_classes 2281 <other_arguments>
```

## Loading extracted I3D features

```python
import lzma
out_file = "path_to_lzma.pkl_file"
with lzma.open(out_file, 'rb') as file:
     raw_data = file.read()
     data = pickle.loads(raw_data)
```


## Reference
Please cite the paper below if you use this code in your research:
```
@article{sincan2024using,
  title={Using an LLM to Turn Sign Spottings into Spoken Language Sentences},
  author={Sincan, Ozge Mercanoglu and Camgoz, Necati Cihan and Bowden, Richard},
  journal={arXiv preprint arXiv:2403.10434},
  year={2024}
}

```
