def get_output_basename(video_path, datasetname=""):
    """
    Extracts the base name (filename without extension) from the given video_path.
    Args:
      video_path (str): Path to the file.

    Returns:
      str: The extracted base name.
    """
    # TODO: Please edit this function according to your directory structure and videos name's format!
    video_name = str(video_path).split("/")[-1]

    if datasetname == "srf":
        # <irrelevant_word>.<video_name>.<extension>, so take the index[1]
        video_name = video_name.split(".")[1]
    elif datasetname in ["focusnews", "bobsl", "meinedgs", "focusnews_wmt", "easier_dgs"]:
        # <video_name>.<irrelevant_word>.<extension>
        video_name = video_name.split(".")[0]
    elif datasetname == "easier_lsf" or datasetname == "srf_dev" or datasetname == "srf_test":
        # <video_name_part1>.<video_name_part2>.<extension>
        video_name = video_name.split(".")[0] + "." + video_name.split(".")[1]
    else:
        print("UNKNOWN dataset name! Entire video name will be used.")


    return video_name

